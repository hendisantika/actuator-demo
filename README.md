# Spring Boot Actuator Demo

##### Run this project by this command : `mvn clean spring-boot:run`

Go to your favorite browser then type : `http://localhost:8080/actuator/info`

##### Explore Actuator Endpoints

All the actuator endpoints will be available at http://localhost:8080/actuator.

Some of the actuator endpoints are protected with Spring Security's HTTP Basic Authentication. You can use the username actuator and password actuator for http basic authentication.

|Endpoint ID| 	Description|
|---|---|
|auditevents| 	Exposes audit events (e.g. auth_success, order_failed) for your application|
|info 	|Displays information about your application.|
|health 	|Displays your application’s health status.|
|metrics 	|Shows various metrics information of your application.|
|loggers 	|Displays and modifies the configured loggers.|
|logfile 	|Returns the contents of the log file (if logging.file or logging.path properties are set.)||
|httptrace 	|Displays HTTP trace info for the last 100 HTTP request/response.|
|env 	|Displays current environment properties.|
|flyway 	|Shows details of Flyway database migrations.|
|liquidbase 	|Shows details of Liquibase database migrations.|
|shutdown 	|Lets you shut down the application gracefully.|
|mappings 	|Displays a list of all @RequestMapping paths.|
|scheduledtasks 	|Displays the scheduled tasks in your application.|
|threaddump 	|Performs a thread dump.|
|heapdump 	|Returns a GZip compressed JVM heap dump.|

### Downloading and Running Prometheus using Docker

### 1. Downloading Prometheus

    You can download the Prometheus docker image using docker pull command like so -

    `$ docker pull prom/prometheus`

    Once the image is downloaded, you can type docker image ls command to view the list of images present locally -
    ```
    $ docker image ls
    REPOSITORY                                   TAG                 IMAGE ID            CREATED             SIZE
    prom/prometheus                              latest              b82ef1f3aa07        5 days ago          119MB
    ```

### 2. Prometheus Configuration (prometheus.yml)

    Next, We need to configure Prometheus to scrape metrics data from Spring Boot Actuator’s /prometheus endpoint.

    Create a new file called prometheus.yml with the following configurations -
    ```
    # my global config
    global:
      scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
      evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
      # scrape_timeout is set to the global default (10s).

    # Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
    rule_files:
      # - "first_rules.yml"
      # - "second_rules.yml"

    # A scrape configuration containing exactly one endpoint to scrape:
    # Here it's Prometheus itself.
    scrape_configs:
      # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
      - job_name: 'prometheus'
        # metrics_path defaults to '/metrics'
        # scheme defaults to 'http'.
        static_configs:
        - targets: ['127.0.0.1:9090']

      - job_name: 'spring-actuator'
        metrics_path: '/actuator/prometheus'
        scrape_interval: 5s
        static_configs:
        - targets: ['HOST_IP:8080']

    ```

    The above configuration file is an extension of the [basic configuration](https://prometheus.io/docs/prometheus/latest/getting_started/#configuring-prometheus-to-monitor-itself) file available in the Prometheus documentation.

    The most important stuff to note in the above configuration file is the spring-actuator job inside scrape_configs section.

    The metrics_path is the path of the Actuator’s prometheus endpoint. The targets section contains the HOST and PORT of your Spring Boot application.

    Please make sure to replace the HOST_IP with the IP address of the machine where your Spring Boot application is running. Note that, localhost won’t work here because we’ll be connecting to the HOST machine from the docker container. You must specify the network IP address.

### 3. Running Prometheus using Docker

    Finally, Let’s run Prometheus using Docker. Type the following command to start a Prometheus server in the background -

    `$ docker run -d --name=prometheus -p 9090:9090 -v <PATH_TO_prometheus.yml_FILE>:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml`

    Please make sure to replace the <PATH_TO_prometheus.yml_FILE> with the PATH where you have stored the Prometheus configuration file.

    After running the above command, docker will start the Prometheus server inside a container. You can see the list of all the containers with the following command -

    ```
    $ docker container ls
    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
    e036eb20b8ad        prom/prometheus     "/bin/prometheus --c…"   4 minutes ago       Up 4 minutes        0.0.0.0:9090->9090/tcp   prometheus
    ```
### 4. Visualizing Spring Boot Metrics from Prometheus dashboard

 That’s it! You can now navigate to http://localhost:9090 to explore the Prometheus dashboard.

 You can enter a Prometheus query expression inside the Expression text field and visualize all the metrics for that query.

 Following are some Prometheus graphs for our Spring Boot application’s metrics -

 * System’s CPU usage -
 * Response latency of a slow API -

 You can check out the official Prometheus documentation to learn more about [Prometheus Query Expressions](https://prometheus.io/docs/introduction/first_steps/#using-the-expression-browser).

### Downloading and running Grafana using Docker

Type the following command to download and run Grafana using Docker -

`$ docker run -d --name=grafana -p 3000:3000 grafana/grafana`

The above command will start Grafana inside a Docker container and make it available on port 3000 on the Host machine.

You can type docker container ls to see the list of Docker containers -
```
$ docker container ls
CONTAINER ID        IMAGE               COMMAND                  CREATED                  STATUS              PORTS                    NAMES
cf9196b30d0d        grafana/grafana     "/run.sh"                Less than a second ago   Up 5 seconds        0.0.0.0:3000->3000/tcp   grafana
e036eb20b8ad        prom/prometheus     "/bin/prometheus --c…"   16 minutes ago           Up 16 minutes       0.0.0.0:9090->9090/tcp   prometheus
```
That’s it! You can now navigate to http://localhost:3000 and log in to Grafana with the default username admin and password admin.

### Configuring Grafana to import metrics data from Prometheus

Follow the steps below to import metrics from Prometheus and visualize them on Grafana:

1. Add the Prometheus data source in Grafana
2. Create a new Dashboard with a Graph
3. Add a Prometheus Query expression in Grafana’s query editor
4. Visualize metrics from Grafana’s dashboard

http://localhost:8080/actuator/info

```json
// 20240503142313
// http://localhost:8080/actuator/info

{
  "app": {
    "name": "actuator-demo",
    "description": "Demo project for Spring Boot",
    "version": "0.0.1-SNAPSHOT",
    "encoding": "UTF-8",
    "java": {
      "version": "21"
    }
  },
  "application": {
    "name": "Actuator info",
    "description": "A demo Spring Project with information"
  },
  "organization": "JVM INDONESIA",
  "java-version": "21",
  "java-vendor": "Amazon.com Inc.",
  "git": {
    "commit": {
      "id": {
        "describe-short": "${git.commit.id.describe-short}",
        "abbrev": "${git.commit.id.abbrev}",
        "full": "${git.commit.id}",
        "describe": "${git.commit.id.describe}"
      },
      "message": {
        "short": "${git.commit.message.short}",
        "full": "${git.commit.message.full}"
      },
      "user": {
        "name": "${git.commit.user.name}",
        "email": "${git.commit.user.email}"
      },
      "time": "${git.commit.time}"
    },
    "branch": "${git.branch}",
    "build": {
      "time": "${git.build.time}",
      "version": "${git.build.version}",
      "host": "${git.build.host}",
      "number": {
        "unique": "${git.build.number.unique}"
      },
      "user": {
        "name": "${git.build.user.name}",
        "email": "${git.build.user.email}"
      }
    },
    "tags": "${git.tags}",
    "closest": {
      "tag": {
        "commit": {
          "count": "${git.closest.tag.commit.count}"
        },
        "name": "${git.closest.tag.name}"
      }
    },
    "remote": {
      "origin": {
        "url": "${git.remote.origin.url}"
      }
    },
    "dirty": "${git.dirty}"
  },
  "java": {
    "version": "21",
    "vendor": {
      "name": "Amazon.com Inc.",
      "version": "Corretto-21.0.0.35.1"
    },
    "runtime": {
      "name": "OpenJDK Runtime Environment",
      "version": "21+35-LTS"
    },
    "jvm": {
      "name": "OpenJDK 64-Bit Server VM",
      "vendor": "Amazon.com Inc.",
      "version": "21+35-LTS"
    }
  },
  "os": {
    "name": "Mac OS X",
    "version": "14.4.1",
    "arch": "aarch64"
  }
}
```
